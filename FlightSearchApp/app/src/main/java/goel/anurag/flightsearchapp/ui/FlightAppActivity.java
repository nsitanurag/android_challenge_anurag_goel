package goel.anurag.flightsearchapp.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import goel.anurag.flightsearchapp.R;
import goel.anurag.flightsearchapp.model.FlightsResponseModel;
import goel.anurag.flightsearchapp.ui.common.DividerItemDecoration;
import goel.anurag.flightsearchapp.utils.Constants;

public class FlightAppActivity extends FragmentActivity implements DownloadFlightsHeadlessFragment.AsyncListener {

    private DownloadFlightsHeadlessFragment mDownloadFragment;

    private TextView mMainTextView;

    private RecyclerView mRecyclerView;

    private FlightsAdapter mFlightsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        View emptyView = findViewById(R.id.emptyView);
        mFlightsAdapter = new FlightsAdapter(FlightAppActivity.this, emptyView);
        mRecyclerView.setAdapter(mFlightsAdapter);

        //Headless Fragment
        FragmentManager fm = getSupportFragmentManager();
        mDownloadFragment = (DownloadFlightsHeadlessFragment) fm.findFragmentByTag(Constants.DOWNLOAD_FLIGHTS_FRAG);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (mDownloadFragment == null) {
            mDownloadFragment = DownloadFlightsHeadlessFragment.
                    newInstance(Constants.FLIGHT_URL);
            fm.beginTransaction().add(mDownloadFragment,
                    Constants.DOWNLOAD_FLIGHTS_FRAG).
                    commit();
        } else if (mDownloadFragment.getResponse() != null) {
            mFlightsAdapter.setData(mDownloadFragment.getResponse());
        }


    }


    @Override
    public void onPreExecute() {

    }

    @Override
    public void onPostExecute(FlightsResponseModel result) {
        mFlightsAdapter.setData(result);

    }

    @Override
    public void onCancelled(FlightsResponseModel result) {
        cleanUp();
    }

    private void cleanUp() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment frag = fm.findFragmentByTag(Constants.DOWNLOAD_FLIGHTS_FRAG);
        fm.beginTransaction().remove(frag).commit();

    }
}
