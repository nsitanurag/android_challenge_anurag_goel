package goel.anurag.flightsearchapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anurag on 01/04/17.
 */
public class Flight {

    public String originCode;
    public String destinationCode;
    public Long departureTime;
    public Long arrivalTime;
    public List<Fare> fares = null;
    public String airlineCode;
    @SerializedName("class")
    @Expose
    public String _class;

    @Override
    public String toString() {
        return "originCode:" + originCode + " , " + "destinationCode:" + destinationCode +
                " , " + "departureTime:" + departureTime + " , " + "arrivalTime:" + arrivalTime +
                " , " +fares.toString() + " , " + "airlineCode:" + airlineCode + " , " +  "class:" +
                _class;
    }
}
