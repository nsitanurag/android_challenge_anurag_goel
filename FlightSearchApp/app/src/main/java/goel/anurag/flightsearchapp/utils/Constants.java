package goel.anurag.flightsearchapp.utils;

/**
 * Created by anurag on 01/04/17.
 */

public class Constants {
    public static final String URL = "url";
    public static final String FLIGHT_URL = "https://firebasestorage.googleapis.com/v0/b/gcm-test-6ab64.appspot.com/o/ixigoandroidchallenge%2Fsample_flight_api_response.json?alt=media&token=d8005801-7878-4f57-a769-ac24133326d6";
    public static final String DOWNLOAD_FLIGHTS_FRAG = "download_flights_as_fragment";
}
