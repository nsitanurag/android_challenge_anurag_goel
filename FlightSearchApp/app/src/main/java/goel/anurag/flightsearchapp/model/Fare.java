package goel.anurag.flightsearchapp.model;

public class Fare {

    public Integer providerId;
    public Integer fare;

    @Override
    public String toString() {
        return "providerId:" + providerId + " , " +
                "fare:" + fare;
    }
}