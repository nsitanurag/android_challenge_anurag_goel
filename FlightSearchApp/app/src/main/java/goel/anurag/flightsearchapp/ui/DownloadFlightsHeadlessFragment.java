package goel.anurag.flightsearchapp.ui;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import goel.anurag.flightsearchapp.model.FlightsResponseModel;
import goel.anurag.flightsearchapp.utils.Constants;

/**
 * Created by anurag on 01/04/17.
 */

public class DownloadFlightsHeadlessFragment extends Fragment {

    private final String TAG = DownloadFlightsHeadlessFragment.class.getSimpleName();

    //reference to the activity that receives the async task callbacks
    private AsyncListener listener;

    private DownloadFlightsTask task;
    private FlightsResponseModel response;

    //Function to create new instance
    public static DownloadFlightsHeadlessFragment newInstance(String url) {
        DownloadFlightsHeadlessFragment downloadFlightsHeadlessFragment = new DownloadFlightsHeadlessFragment();
        Bundle args = new Bundle();
        args.putString(Constants.URL, Constants.FLIGHT_URL);
        downloadFlightsHeadlessFragment.setArguments(args);
        return downloadFlightsHeadlessFragment;
    }

    //Called to do initial creatioObjectn of fragment
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        try {
            URL url = new URL(getArguments().getString(Constants.URL));
            task = new DownloadFlightsTask();
            task.execute(url);
        } catch (MalformedURLException e) {
            Log.e(TAG, e.getMessage(), e);
        }

    }

    // Called when an activity is attached
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (AsyncListener) activity;
    }

    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    // Cancel the download
    public void cancel() {
        if (task != null) {
            task.cancel(false);
        }
    }

    public FlightsResponseModel getResponse() {
        return response;
    }


    private class DownloadFlightsTask extends AsyncTask<URL, Void, FlightsResponseModel> {

        @Override
        protected void onPreExecute() {
            if (listener != null)
                listener.onPreExecute();
        }

        @Override
        protected void onPostExecute(FlightsResponseModel result) {
            if (listener != null)
                listener.onPostExecute(result);
        }

        @Override
        protected void onCancelled(FlightsResponseModel result) {
            if (listener != null)
                listener.onCancelled(result);

        }

        @Override
        protected FlightsResponseModel doInBackground(URL... params) {
            StringBuilder result = new StringBuilder();

            HttpURLConnection urlConnection = null;
            try {
                URL url = params[0];
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null) {
                    urlConnection.disconnect();
                    urlConnection = null;
                }
            }

            Type typeOfT = new TypeToken<FlightsResponseModel>() {
            }.getType();

            response = new Gson().fromJson(result.toString(), typeOfT);

            return response;
        }
    }


    public interface AsyncListener {
        void onPreExecute();

        void onPostExecute(FlightsResponseModel result);

        void onCancelled(FlightsResponseModel result);
    }

}