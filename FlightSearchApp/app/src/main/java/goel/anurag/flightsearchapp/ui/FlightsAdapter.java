package goel.anurag.flightsearchapp.ui;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import goel.anurag.flightsearchapp.R;
import goel.anurag.flightsearchapp.model.Appendix;
import goel.anurag.flightsearchapp.model.Fare;
import goel.anurag.flightsearchapp.model.Flight;
import goel.anurag.flightsearchapp.model.FlightsResponseModel;

/**
 * Created by anurag on 01/04/17.
 */

public class FlightsAdapter extends RecyclerView.Adapter<FlightViewHolder> {

    private Context mContext;
    private final View mEmptyView;

    private ArrayList<Flight> flights;
    private Appendix appendix;

    public FlightsAdapter(Context context, View emptyView) {
        this.mContext = context;
        this.mEmptyView = emptyView;

    }

    public void setData(FlightsResponseModel model)
    {
        this.flights = model.flights;
        this.appendix = model.appendix;
        notifyDataSetChanged();
    }

    @Override
    public FlightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (parent instanceof RecyclerView) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_list_item, parent, false);
            FlightViewHolder holder = new FlightViewHolder(view);
            //findAndCacheViews(holder);
            return holder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(FlightViewHolder holder, int position) {
        Flight flight = flights.get(position);
        Date dd = new java.util.Date(flight.departureTime);
        String deptime = new SimpleDateFormat("h:mm").format(dd);
        dd = new java.util.Date(flight.arrivalTime);
        String arrtime = new SimpleDateFormat("h:mm").format(dd);
        holder.mDepTime.setText(deptime);
        holder.mArrTime.setText(arrtime);
        holder.mAirline.setText(appendix.airlines.get(flight.airlineCode));
        holder.mTravelClass.setText(flight._class);

        List<Fare> fares = flight.fares;
        int pos = 0;
        int fare = fares.get(0).fare;
        for(int i = 1 ; i < fares.size(); i++) {
            if(fares.get(i).fare < fare) {
                pos = i;
                fare = fares.get(i).fare;
            }
        }

        holder.mProvider.setText(appendix.providers.get(String.valueOf(flight.fares.get(pos).providerId)));
        holder.mPrice.setText(String.valueOf(flight.fares.get(pos).fare));
    }

    @Override
    public int getItemCount() {
        if (flights != null) {
            return flights.size();
        }
        return 0;
    }
}
