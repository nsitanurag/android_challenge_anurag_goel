package goel.anurag.flightsearchapp.model;

import java.util.Map;

/**
 * Created by anurag on 01/04/17.
 */

public class Appendix {
    public Map<String, String> airlines;
    public Map<String, String> airports;
    public Map<String, String> providers;

    @Override
    public String toString() {
        return airlines.toString() + " " + airports.toString() + " " + providers.toString();
    }
}
