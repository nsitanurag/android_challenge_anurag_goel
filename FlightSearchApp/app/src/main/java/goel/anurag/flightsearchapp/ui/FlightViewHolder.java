package goel.anurag.flightsearchapp.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import goel.anurag.flightsearchapp.R;

/**
 * Created by anurag on 01/04/17.
 */

public class FlightViewHolder extends RecyclerView.ViewHolder {

    public TextView mDepTime;
    public TextView mArrTime;
    public TextView mAirline;
    public TextView mTravelClass;
    public TextView mProvider;
    public TextView mPrice;

    public FlightViewHolder(View itemView) {
        super(itemView);
        mDepTime = (TextView) itemView.findViewById(R.id.depTime);
        mArrTime = (TextView) itemView.findViewById(R.id.arvTime);
        mAirline = (TextView) itemView.findViewById(R.id.airlines);
        mTravelClass = (TextView) itemView.findViewById(R.id.travelclass);
        mProvider = (TextView) itemView.findViewById(R.id.provider);
        mPrice = (TextView) itemView.findViewById(R.id.price);
    }
}
