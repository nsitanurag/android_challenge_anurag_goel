package goel.anurag.flightsearchapp.model;

import java.util.ArrayList;

/**
 * Created by anurag on 01/04/17.
 */

public class FlightsResponseModel {
    public Appendix appendix;
    public ArrayList<Flight> flights = null;

    @Override
    public String toString() {
        return " { " + appendix.toString() + " } \n { " + flights.toString();

    }
}
